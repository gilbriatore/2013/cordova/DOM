function manipularElementos(){
    var pai = document.getElementById("pai");
    console.log("No. filhos: " + pai.childElementCount);
    
    var filho1 = pai.firstElementChild;
    console.log(filho1.innerHTML);
    
    var filho3 = pai.lastElementChild;
    console.log(filho3.innerHTML);
    
    //var filho2 = filho1.nextElementSibling;
    var filho2 = filho3.previousElementSibling;
    console.log(filho2.innerHTML);
    
    //pai.removeChild(filho2);
    
    //var filho4 = document.createElement("li");
    //var texto = document.createTextNode("Item 4");
    //filho4.appendChild(texto);    
    //pai.appendChild(filho4);
    
    for(x = 1; x < 20; x++){
      criarItemLista(pai, "Item " + x);    
    }        
    //console.log("passou por aqui!");        
}    

function criarItemLista(elemento, txt){
    var filho = document.createElement("li");
    var texto = document.createTextNode(txt);
    filho.appendChild(texto);    
    elemento.appendChild(filho);
}

window.onload = function(){       
  var bt = document.getElementById("btn");
  bt.onclick = manipularElementos;
}